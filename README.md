# CATracker Frontent
Web Frontend für CATracker ein "Full Stack" Sensor IOT Projekt

## TODO
* Teil 1
    - [X] Setup ReactJS
    - [X] API Request um Positionsdaten zu erhalten
    - [X] Setup Map SDK
    - [X] Positionsdaten anzeigen
        - [ ] Zeitleiste
        - [ ] Infokarte
    - [X] Geräte Daten anzeigen
* Teil 2
    - [ ] Registrierungsformular
    - [ ] Login Formular
    - [ ] Geräte Registrierung
    - [ ] Gerätespezifische Positionsdaten
* Teil 3
    
    - [ ] Heatmap