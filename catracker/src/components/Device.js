// @flow

import React, { Component } from 'react';
import { Card, Button, Form, FormGroup, Label, Input} from 'reactstrap';

export default class Device extends Component {

    state = {
        dev_idInput: String,
        nameInput: String,
        dev_numberInput: String,
        showCard: true
    }

    onChangeEvent(event) {
        var nextState = {};
        nextState[event.target.id] = event.target.value;
        this.setState(nextState);
    }

    doAddDevice(event) {
        event.preventDefault();
        
        var form = new FormData()
        form.set("dev_id", this.state.dev_idInput)
        form.set("dev_number", this.state.dev_numberInput)
        form.set("name", this.state.nameInput)
        fetch('https://platinenmacher.tech/api/v1.1/device/'+this.props.sessionKey, {
            method: 'POST',
            body: form
        }).then((response) => response.json()).then(data => {
            if (data.status === "OK") {
                this.props.updateDeviceList();
            }
        })
    }

    hideCard(event) {
        event.preventDefault();
        this.props.updateDeviceList();
    }

    render() {
        return (
            <Card className="registerCard">
                <Form onSubmit={(event) => {this.doAddDevice(event)}}>
                    <FormGroup>
                        <Label for="nameInput">Geräte Name</Label>
                        <Input name="name" id="nameInput" placeholder="Name" onChange={(event) => {this.onChangeEvent(event)}} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="dev_idInput">Geräte Seriennummer</Label>
                        <Input id="dev_idInput" name="dev_id" type="text" placeholder="Device Serial" onChange={(event) => {this.onChangeEvent(event)}} />
                    </FormGroup>
                    <FormGroup>
                        <Label for="dev_numberInput">Geräte ID</Label>
                        <Input id="dev_numberInput" name="dev_number" type="text" placeholder="Device ID" onChange={(event) => {this.onChangeEvent(event)}} />
                    </FormGroup>
                        <FormGroup><Button>Submit</Button> <Button onClick={(event) => {this.hideCard(event)}}>Close</Button></FormGroup>
                </Form>
            </Card>
        )
    }
}