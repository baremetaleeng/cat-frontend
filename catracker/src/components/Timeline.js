// @flow

import React, { Component } from 'react';
import { Range } from 'rc-slider';
import 'rc-slider/assets/index.css';
import {Container, Row, Col, Card} from 'reactstrap';

import Calendar from 'react-calendar'
import 'react-calendar/dist/Calendar.css';

export default class TrackerMap extends Component {

  constructor(props){
    super(props)
    this.props = props
}
  state = {
    date: new Date(),
    showCalender: Boolean,
    sliderValue: [],
  }

  onCalendarChange = (date) => {
    this.setState({date: date, 
      showCalender: false,
     }); 
     this.props.timeline.date = date
     this.props.TimelineCallback(null);
  }
  onRangeChange = (value) => {
    this.setState({
      sliderValue: value,
     }); 
    this.props.MapMarkerUpdateCallback(value);
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.timeline.sliderMax !== this.props.timeline.sliderMax){
      this.setState({
        sliderValue: [0, nextProps.timeline.sliderMax]
      })
    }
    if(nextProps.slider_pos !== this.state.sliderValue){
      console.log(nextProps.slider_pos)
      this.setState({sliderValue: nextProps.slider_pos})
    }
  }


  componentDidMount(){
    this.setState({showCalender: false, sliderValue: [0,0]});
  }

render () {
  const sliderMax = this.props.timeline.sliderMax;
  var rangeText = ""
  if(sliderMax > 0){
   rangeText = <>{this.state.sliderValue[0]+1} bis {this.state.sliderValue[1]}</>
  }
    return(
      <Container className="timeline">
        <Row>
          <Col xs="3">
            {this.state.showCalender ? 
              <Calendar onChange={this.onCalendarChange} value={this.state.date} /> 
              :
              <Card className="footer" onClick={()=>{this.setState({showCalender: true})}}>{this.state.date.toLocaleDateString()} &nbsp;
              {rangeText}
              </Card>
            }
          </Col>
          <Col xs="9" className="timelineSlider">
            <Range value={this.state.sliderValue} defaultValue={[this.state.sliderMin,sliderMax]} 
              min={0} max={sliderMax} step={1} onChange={this.onRangeChange}/>
          </Col>        
        </Row>
      </Container>
    );
}
}