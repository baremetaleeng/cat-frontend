// @flow

import React, { Component } from 'react';
import { Chart } from 'react-charts'

type State = {
    tracking: Boolean,
    hdop: Number,
    gps_perm: Boolean,
    duty_cycle: Number,
    signal: Boolean
}

export default class BatteryChart extends Component<{}, State> {

  render() {
    let min = 0
    let max = 0
    let graphData = []
    if (this.props.battery){
      for (const entry of this.props.battery){
        if (min > entry.voltage) {
          min = entry.voltage
        }
        if (max < entry.voltage) {
          max = entry.voltage
        }
        graphData.push([entry.date, entry.voltage])
      }
    }
    
    const data = [
          {
            label: 'Batterie',
            data: graphData
          }
        ]
     
    const axes = [
          { primary: true, type: 'time', position: 'bottom', show: true },
          { type: 'linear', position: 'left', range: [min+10, max+10], show: true }
        ]
      
        return(
            <div
            style={{
              width: '300px',
              height: '200px'
            }}
          >
            <Chart data={data} axes={axes} tooltip/>
          </div>
        );
    }
}