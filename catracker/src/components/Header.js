import React, { Component } from 'react';
import { Button, ButtonGroup, ButtonToolbar } from 'reactstrap';

import 'react-tabs/style/react-tabs.css';

export default class TabHeader extends Component<> {

  constructor(props) {
    super(props)
    this.state = {
      selectedButton: 0
    }
  }

  async onClick(index, dev_id) {
    this.setState({"selectedButton": index})
    this.props.MapCallback(dev_id);
    setTimeout(() => {
      this.onClick(index, dev_id);
    }, 60000);
  }

  render() {
    const buttons = []
    if (this.props.devices) for (const [index, dev] of this.props.devices.entries()) {
    
      if(index === this.state.selectedButton) {
        buttons.push(<Button id={index} key={index} onClick={() => {this.onClick(index, dev.dev_id)}}>{dev.name}</Button>)
      } else {
        buttons.push(<Button id={index} key={index} outline onClick={() => {this.onClick(index, dev.dev_id)}}>{dev.name}</Button>)
      }
    }
    return (
      <ButtonToolbar className="header">
      <ButtonGroup>
        {buttons}
      </ButtonGroup>
      </ButtonToolbar>
      );
  }
}