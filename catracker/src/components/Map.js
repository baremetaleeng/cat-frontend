// @flow

import React, { Component } from 'react';
import { Map, TileLayer, CircleMarker, Popup } from 'react-leaflet'

type State = {
  lat: number,
  lng: number,
  zoom: number,
}

export default class TrackerMap extends Component<{}, State> {

  render() {
    const position = [this.props.lat, this.props.lng]
    const positions = []

    if (this.props.pos.length > 0){
      for (const [index, value] of this.props.pos.entries()) {
        const pos = [value.latitude, value.longitude];
        var date = new Date(value.date)
        const time = date.getHours() + ":" + date.getMinutes();
        const dev_id = value.device_id;
        const hdop = value.hdop;
        const altitude = value.altitude;
        const satellites = value.satellites;
          positions.push(<CircleMarker center={pos} radius="9" key={index}>
          <Popup>
          <p>
            <h3>Position {index+1}</h3> 
            <div style={{float: "right"}}>{time}Uhr</div>
          </p>
          <p>Höhe: {altitude}</p>
          <p>HDOP: {hdop}</p>
          <p>Longitude: {pos[0]}</p>
          <p>Latitude: {pos[1]}</p>
          <p>Satelliten: {satellites}</p>
          </Popup>
        </CircleMarker>);
      }
  }  
    return (
      <Map className="map" center={position} zoom={this.props.zoom}>
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {positions}
      </Map>
    )
  }

  updateMap(positions) {
    this.setState(positions);
  }
}

