// @flow

import React, { Component } from 'react';
import { Card, CardBody, CardText, Button, ButtonGroup, } from 'reactstrap';
import Slider, {  } from 'rc-slider';

export default class Status extends Component {
    constructor(props){
        super(props)
        this.props = props
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.status.min_hdop !== this.state.hdop){
            if (typeof(nextProps.status.min_hdop) === "number") {
                this.setState({hdop: nextProps.status.min_hdop/10})
            }
        }
        if(nextProps.status.send_frequency !== this.state.send_frequency){
            if (typeof(nextProps.status.send_frequency) === "number") {
                this.setState({send_frequency: nextProps.status.send_frequency})
            } else {
                this.setState({send_frequency: 6})
            }
        }
        if(nextProps.status.tracking !== this.state.track){
            if (typeof(nextProps.status.tracking) === "boolean") {
                this.setState({track: nextProps.status.tracking})
            } else {
                this.setState({track: false})
            }
        }
        if(nextProps.status.perm_on !== this.state.perm_on){
            if (typeof(nextProps.status.gps_perm) === "boolean") {
                this.setState({perm_on: nextProps.status.gps_perm})
            } else {
                this.setState({perm_on: false})
            }
        }
        if(nextProps.status.signal !== this.state.signal){
            if (typeof(nextProps.status.signal) === "boolean") {
                this.setState({signal: nextProps.status.signal})
            } else {
                this.setState({signal: false})
            }
        }
        if(nextProps.status !== this.state.status){
            if (nextProps.status.battery) { 
                var timestamp = Date.parse(nextProps.status.battery.date);
                this.setState({
                    lastUpdate: new Date(timestamp).toLocaleDateString(undefined, { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' })
                }) 
            }
        }
    }

    state = {
        hdop: Number,
        send_frequency: Number,
        showStatus: Boolean,
        track: false,
        perm_on: false,
        signal: false,
        lastUpdate: "?"
    }    
    postForm(){
        var form = new FormData()
        form.set("track", this.state.track)
        form.set("perm_on", this.state.perm_on)
        form.set("signal", this.state.signal)
        form.set("hdop", this.state.hdop)
        form.set("send_frequency", this.state.send_frequency)
        fetch('https://platinenmacher.tech/api/v1.1/status/'+this.props.status.sessionKey+'/'+this.props.status.dev_id, {
            method: 'POST',
            body: form
        }).then((response) => console.log(response))
    }

    onHDOPChange(value) {
        this.setState({
          hdop: value,
        });
    }

    onSendFrequencyChange(value) {
        this.setState({
            send_frequency: value
        })
    }

    async onTrackingClick(sw) {
        await this.setState({track: sw})
        this.postForm()
    }
    
    async onGPSClick(sw) {
        await this.setState({perm_on: sw})
        this.postForm()
    }

    async onSignalClick(sw) {
        await this.setState({signal: sw})
        this.postForm()
    }

    componentDidMount() {
        this.setState({
            hdop: 0,
            send_frequency: 0,
            showStatus: false
        })
    }

    showStatus(show){
        this.setState({showStatus: show})
    }

    render() {
        const active = this.state.track
        const perm_on = this.state.perm_on
        const signal = this.state.signal
        const charging = this.props.status.charging
        const lastDate = this.state.lastUpdate

        const batMax = 3243
        const batMin = 2609
        const batDiff = batMax - batMin
        var batteryPercent = "?"
        if(this.props.status.battery) {
        batteryPercent = Math.floor(100 * (this.props.status.battery.voltage - batMin) / batDiff) + 1
        }
        if (batteryPercent > 100)
            batteryPercent = 100;

        if (this.state.showStatus){
        return (<>
            <Button className="statusButton" color="info" onClick={() => {this.showStatus(false)}}>Status</Button>
            <Card className="status">
                <CardBody>
                <CardText>
                    <small>{lastDate}</small> <br/>
                    {charging ? "Laden": "Batteriebetrieb" } &nbsp;
                    <b>{batteryPercent}%</b>
               </CardText>
                </CardBody>
                <CardBody>
                Tracking <ButtonGroup>
                    <Button id="1" key="1" outline={!active} onClick={() => {this.onTrackingClick(true)}}>ON</Button>
                    <Button id="2" key="2" outline={active} onClick={() => {this.onTrackingClick(false)}}>OFF</Button>
                </ButtonGroup>
                </CardBody>
                <CardBody>
                HDOP {this.state.hdop}
                <div>
                    <Slider className="statusSlider" min={1} max={6} step={.1}  value={this.state.hdop} 
                            onChange={(val) => {this.onHDOPChange(val)}}
                            onAfterChange={() => {this.postForm()}} /> 
                </div>
                </CardBody>
                <CardBody>
                GPS <ButtonGroup>
                    <Button id="1" key="1" outline={!perm_on} onClick={() => {this.onGPSClick(true)}}>Permanent</Button>
                    <Button id="2" key="2" outline={perm_on} onClick={() => {this.onGPSClick(false)}}>Periodic</Button>
                </ButtonGroup>
                </CardBody>
                <CardBody>
                Übertragung alle {this.state.send_frequency * 10} Sekunden
                <div>
                    <Slider className="statusSlider" min={1} max={255} step={1} value={this.state.send_frequency} 
                            onChange={(val) => {this.onSendFrequencyChange(val)}} 
                            onAfterChange={() => {this.postForm()}} />
                </div>
                </CardBody>
                <CardBody>
                Signal <ButtonGroup>
                    <Button id="1" key="1" outline={!signal} onClick={() => {this.onSignalClick(true)}}>ON</Button>
                    <Button id="2" key="2" outline={signal} onClick={() => {this.onSignalClick(false)}}>OFF</Button>
                </ButtonGroup>
                </CardBody>
            </Card>
            </>
        );
        } else {
            return(
                <Button className="statusButton" color="info" onClick={() => {this.showStatus(true)}}>Status</Button>
            );
        }
    }
}