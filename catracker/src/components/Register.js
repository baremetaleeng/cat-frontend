// @flow

import React, { Component } from 'react';
import { Card, Button, Form, FormGroup, Label, Input} from 'reactstrap';

export default class Register extends Component {

    state = {
        register: false,
        usernameInput: String,
        passwordInput: String,
        emailInput: String,
        showInfo: false,
        infoText: String,
        showCard: true
    }

    showRegister() {
        this.setState({register: true})
    }

    hideRegister() {
        this.setState({register: false})
    }

    onChangeEvent(event) {
        var nextState = {};
        nextState[event.target.id] = event.target.value;
        this.setState(nextState);
    }

    doRegister(event) {
        event.preventDefault();
        
        var form = new FormData()
        form.set("user", this.state.usernameInput)
        form.set("password", this.state.passwordInput)
        if(this.state.register){
            form.set("email", this.state.emailInput)
            fetch('https://platinenmacher.tech/api/v1.1/register', {
                method: 'POST',
                body: form
            }).then((response) => console.log(response))
        } else {
            fetch('https://platinenmacher.tech/api/v1.1/login', {
                method: 'POST',
                body: form
            }).then((response) => response.json()).then(data => {
                if (data.sessionKey) {
                    this.props.LoginCallback(data.sessionKey)
                    this.setState({infoText : "Login OK", showInfo: true})
                    setTimeout(() => {
                        this.setState({showInfo: false, showCard: false});
                      }, 1000);
                } else {
                    this.setState({infoText : data.error, showInfo: true})
                    setTimeout(() => {
                        this.setState({showInfo: false});
                      }, 1000);
                }
            })
        }
    }

    render() {
        if(this.state.showCard && this.props.sessionKey === "") {
            if(this.state.register) {
            return (
                <Card className="registerCard">
                    <Form onSubmit={(event) => {this.doRegister(event)}}>
                        <FormGroup>
                            <Label for="usernameInput">Username</Label>
                            <Input id="usernameInput" name="user" type="text" placeholder="Username" onChange={(event) => {this.onChangeEvent(event)}} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="passwordInput">Password</Label>
                            <Input type="password" name="password" id="passwordInput" placeholder="Password" onChange={(event) => {this.onChangeEvent(event)}} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="emailInput">Email</Label>
                            <Input id="emailInput" name="email" type="email" placeholder="Email" onChange={(event) => {this.onChangeEvent(event)}} />
                        </FormGroup>
                            <FormGroup><Button>Submit</Button> <Button onClick={() => {this.hideRegister()}}>Login</Button></FormGroup>
                    </Form>
                </Card>
            )} else {
                return(
                <Card className="registerCard">
                    <Form onSubmit={(event) => {this.doRegister(event)}}>
                        <FormGroup>
                            <Label for="usernameInput">Username</Label>
                            <Input id="usernameInput" name="user" type="text" placeholder="Username" onChange={(event) => {this.onChangeEvent(event)}} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="passwordInput">Password</Label>
                            <Input type="password" name="password" id="passwordInput" placeholder="Password" onChange={(event) => {this.onChangeEvent(event)}} />
                        </FormGroup>
                        {this.state.showInfo ?
                            <p>{this.state.infoText}</p>
                            :   
                            <FormGroup><Button>Submit</Button> <Button onClick={() => {this.showRegister()}}>Register</Button></FormGroup>
                        }
                    </Form>
                </Card>
            )}
        } else {
            return(<></>)
        }
    }
}