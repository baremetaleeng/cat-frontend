import React, { Component } from 'react';
import { Card, CardText } from 'reactstrap';
import TrackerMap from './Map';
import TabHeader from './Header';
import Timeline from './Timeline';
import Status from './Status';
import Device from './Device';

import {fetchDevicePositionsAt, fetchDeviceStatus, fetchDeviceList } from '../api'
import Register from './Register';

export default class App extends Component{

    constructor(props) {
        super(props)
        this.loadPositions = this.loadPositions.bind(this)
        this.setSessionKey = this.setSessionKey.bind(this)
        this.updateDeviceList = this.updateDeviceList.bind(this)
        this.updateMapMarker = this.updateMapMarker.bind(this)
    }

    state = {
      pos: [],
      selected_pos: [],
      lat: 51.163361,
      lng: 10.447683,
      zoom: 6,
      status: {},
      timeline: {"date": new Date(), "sliderMax": 0},
      dev_id: "",
      sessionKey: "",
      devices: [],
      addDevice: false,
      slider_pos: [0,0]
    }

    async loadPositions(dev_id) {
      if(!dev_id){
        dev_id = this.state.dev_id
      } else {
        this.setState({"dev_id":dev_id})
      }
      fetchDeviceStatus(this.state.sessionKey, dev_id).then((stat) => {this.setState({"status":stat})});
      fetchDevicePositionsAt(this.state.sessionKey, dev_id, this.state.timeline.date, this.state.timeline.hours).then((pos) => {
        if(pos.length > 0){
          this.setState({
            pos: pos,
            lat: pos[0].latitude,
            lng: pos[0].longitude,
            zoom: 23,
            timeline: {...this.state.timeline, "sliderMax": pos.length}
          })
        } else {
          this.setState({
            pos: []
          })
        }
        return pos.length
      }).then((len) => {
        this.updateMapMarker([0, len-1])
        this.setState({slider_pos: [0, len]})
      })      
      
    }

    setSessionKey(key) {
      this.setState({"sessionKey": key})
      localStorage.setItem("sessionKey", key);
      fetchDeviceList(key).then((devs) => {this.setState({"devices": devs})});
    }

    updateDeviceList() {
      this.setState({
        addDevice: false
      })
      if(localStorage.getItem("sessionKey")){
        this.setState({"sessionKey": localStorage.getItem("sessionKey")})
        fetchDeviceList(localStorage.getItem("sessionKey")).then((devs) => {this.setState({"devices": devs})});
      }
    }

    componentDidMount() {
      this.updateDeviceList()
      navigator.geolocation.getCurrentPosition(
        (position)=>{
          this.setState({
            lat: position.coords.latitude,
            lng: position.coords.longitude,
            zoom: 23
        })}, 
        (err) => {console.log(err)})

    }

    updateMapMarker(range) {
      const newpos = this.state.pos.slice(range[0],range[1])
      this.setState({
        selected_pos : newpos,
        slider_pos: range
      })
    }

    logoutClick(){
      localStorage.removeItem("sessionKey");
    }

    showAddDevice(){
      this.setState({"addDevice": true});
    }

    render () {
        const positions = this.state.selected_pos
        const latitude = this.state.lat
        const longitude = this.state.lng
        const zoom = this.state.zoom
        const status = {...this.state.status, "dev_id":this.state.dev_id, "sessionKey":this.state.sessionKey}
        const timeline = this.state.timeline
        const slider_pos = this.state.slider_pos
        var logout;
        var addDevice;
        if(this.state.sessionKey){
          logout = (<a onClick={() => this.logoutClick()} href="/">logout</a>);
          addDevice = (<a onClick={() => this.showAddDevice()} href="#">Gerät hinzufügen</a>);
        }
        var addDeviceCard;
        if(this.state.addDevice){
          addDeviceCard = (<Device sessionKey={this.state.sessionKey} updateDeviceList={this.updateDeviceList}/>);
        } 
        return(<>
              <Register sessionKey={this.state.sessionKey} LoginCallback={this.setSessionKey} />
              {addDeviceCard}
              <TrackerMap pos={positions} lat={latitude} lng={longitude} zoom={zoom}/>
              <TabHeader devices={this.state.devices} MapCallback={this.loadPositions}/>
              <Timeline TimelineCallback={this.loadPositions} 
                        MapMarkerUpdateCallback={this.updateMapMarker} timeline={timeline} slider_pos={slider_pos}/>
              <Status status={status} />
              <Card className="footer">
        <CardText>Made by <a href="http://www.platinenmacher.tech">Platinenmacher</a> {logout} {addDevice}</CardText>
              </Card>
              </>
        );
    }
    }