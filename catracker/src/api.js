
export async function fetchDevicePositions(sessionKey, dev_id) {
    var ret = JSON.parse("{}")
    var data = await fetch('https://platinenmacher.tech/api/v1.1/pos/'+sessionKey+'/'+dev_id).catch((e) => {console.log(e)})
    data = await data.json()
    if (data.pos && data.pos.length > 0) {
        ret.pos = data.pos;
        ret.lat = data.pos.slice(-1)[0].latitude;
        ret.lng = data.pos.slice(-1)[0].longitude;
    }
    return ret
}

export async function fetchDevicePositionsAt(sessionKey, dev_id, date) {
    var ret = JSON.parse("{}")
    var dateStr = date.getFullYear() + ('0' + (date.getMonth()+1)).slice(-2) + ('0' + date.getDate()).slice(-2)
    var from = dateStr + '000000';
    var to = dateStr + '235959';
    var data = await fetch('https://platinenmacher.tech/api/v1.1/pos/'+sessionKey+'/'+dev_id+'?from='+from+'&to='+to).catch((e) => {console.log(e)})
    data = await data.json()
    //console.log(data)
    if (data.pos && data.pos.length > 0) {
        ret = data.pos;
    } else {
        ret = [];
    }
    return ret
}

export async function fetchDeviceStatus(sessionKey, dev_id) {
    var data = await fetch('https://platinenmacher.tech/api/v1.1/status/'+sessionKey+'/'+dev_id).catch((e) => {console.log(e)})
    return data.json()
}

export async function fetchDeviceList(sessionKey) {
    var data = await fetch('https://platinenmacher.tech/api/v1.1/device/'+sessionKey).catch((e) => {console.log(e)})
    var j = await data.json()
    var devs = []
    for (const dev of j.devices){
        devs.push(dev)
    }
    return devs
}
